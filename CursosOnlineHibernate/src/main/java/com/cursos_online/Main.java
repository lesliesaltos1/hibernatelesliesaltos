package com.cursos_online;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.cursos_online.entidades.Curso;
import com.cursos_online.entidades.Estudiantes;

public class Main {

		static final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
				.configure() // configures settings from hibernate.cfg.xml
				.build();
		
		static SessionFactory sessionFactory = new MetadataSources( registry ).buildMetadata().buildSessionFactory();
		
		
		public static void main(String[] args) {
			Curso cur1 =new Curso("topicos");
			Curso cur2 =new Curso("fundamentos programacion");
			
			
			ingresarCursos(cur1);
			ingresarCursos(cur2);
			
			
			Estudiantes est1 =new Estudiantes("Leslie","saltos");
			
		     ingresarEstudiantes(est1);
			
		     Estudiantes est2 =new Estudiantes("kiki","chan");
				
		     ingresarEstudiantes(est2);
		     
		     System.out.println("--------------------Metodos");
				
		     eliminarEstudiante(3);
		     
		    
		     
			
		    
			
		}
		


	static List<Estudiantes> getEstudiantesPorNombre(String name){
		Session session = sessionFactory.openSession();
		Query query = session
				.createQuery("from Estudiantes where nombre=:name");
		query.setParameter("name",name);
		@SuppressWarnings("unchecked")
		List<Estudiantes>estudiante=(List<Estudiantes>) query.getResultList();
		return estudiante;
	}
	
	static Estudiantes getEstudiantesPorId(int id){
		Session session = sessionFactory.openSession();
		Query query = session
				.createQuery("from Estudiantes where id=:id");
		query.setParameter("id",id);
		@SuppressWarnings("unchecked")
		List<Estudiantes>estudiante=(List<Estudiantes>) query.getResultList();
		if(estudiante.size()!=0) {
			return estudiante.get(0);
		}
		
		return null;
	}
	
	static Curso getCursosPorId(int id){
		Session session = sessionFactory.openSession();
		Query query = session
				.createQuery("from Curso where id=:id");
		query.setParameter("id",id);
		@SuppressWarnings("unchecked")
		List<Curso>cursos=(List<Curso>) query.getResultList();
		if(cursos.size()!=0) {
			return cursos.get(0);
		}
		
		return null;
	}
	
	
	static void ingresarEstudiantes(Estudiantes estudiantes){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(estudiantes);
		
		session.getTransaction().commit();
		session.close();
		
		
	}
	
	static void ingresarCursos(Curso c){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(c);
		
		session.getTransaction().commit();
		session.close();
		
		
	}
	
	static List<Curso>getCursos(){
		Session session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<Curso>cursos=(List<Curso>) session.createQuery("from Curso",Curso.class);
		return cursos;
	}
	
		
	
	
	static void modificarCurso(Curso curso) {
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		//Query query = session
			//	.createQuery("Update Estudiantes set descripcion = "+ curso.getDescripcion()
				//		+ "+ where id=:"+curso.getId());
		session.merge(curso);
		session.getTransaction().commit();
		session.close();
		
		
		
	}
	static void eliminarCurso(int id) {
		Curso cur = getCursosPorId(id);
		if(cur!=null) {
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			session.delete(cur);
			session.getTransaction().commit();
			session.close();
		}else {
			System.out.println("No existe curso con ID= "+ id);
		}
		
	}
	

	static void modificarEstudiante(Estudiantes est) {
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.merge(est);
		session.getTransaction().commit();
		session.close();
		
		
	}
	static void eliminarEstudiante(int id) {
		Estudiantes est = getEstudiantesPorId(id);
		if(est!=null) {
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			session.delete(est);
			session.getTransaction().commit();
			session.close();
		}else {
			System.out.println("No existe estudiante con ID= "+ id);
		}
		
	}
	
	

	
	
	

}
